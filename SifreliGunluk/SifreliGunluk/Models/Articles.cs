﻿using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SifreliGunluk.Models
{
    public class Articles
    {
        string id;
        string username;
        string article;
        string title;
        string date;
        [JsonProperty(PropertyName ="id")]
        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        [JsonProperty(PropertyName ="username")]
        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        [JsonProperty(PropertyName ="article")]
        public string Article
        {
            get { return article; }
            set { article = value; }
        }
        [JsonProperty(PropertyName ="title")]
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        [JsonProperty(PropertyName ="date")]
        public string Date
        {
            get { return date; }
            set { date = value; }
        }
        [Version]
        public string Version { get; set; }
    }
}
