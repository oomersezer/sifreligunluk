﻿using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SifreliGunluk.Models
{
    public class User
    {
        string id;
        string name;
        string surname;
        string username;
        string password;
        string gender;
        string age;
        string mail;
        bool done;
        // Veri tabanı columnlarını oluşturuyorum

        [JsonProperty(PropertyName ="id")]
        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        [JsonProperty(PropertyName ="name")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        [JsonProperty(PropertyName ="surname")]
        public string Surname
        {
            get { return surname; }
            set { surname = value; }
        }
        [JsonProperty(PropertyName ="username")]
        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        [JsonProperty(PropertyName ="password")]
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        [JsonProperty(PropertyName ="age")]
        public string Age
        {
            get { return age; }
            set { age = value; }
        }
        [JsonProperty(PropertyName ="gender")]
        public string Gender
        {
            get { return gender; }
            set { gender = value; }
        }
        [JsonProperty(PropertyName ="mail")]
        public string Mail
        {
            get { return mail; }
            set { mail = value; }
        }
        [JsonProperty(PropertyName ="complete")]
        public bool Done
        {
            get { return done; }
            set { done = value; }
        }
        [Version]
        public string Version { get; set; }
    }
}
