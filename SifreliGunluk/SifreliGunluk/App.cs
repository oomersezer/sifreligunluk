﻿using SifreliGunluk.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SifreliGunluk
{
    public class App : Application
    {
        public App()
        {
            MainPage = new GirisSayfa();
        }
    }
}
