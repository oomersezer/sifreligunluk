﻿using SifreliGunluk.Models;
using SifreliGunluk.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SifreliGunluk.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class YeniYaziEkleSayfa : ContentPage
	{
        ArticleManager manager;
		public YeniYaziEkleSayfa ()
		{
            manager = ArticleManager.DefaultManager;
			InitializeComponent ();
		}

        private void buttonKaydet_Clicked(object sender, EventArgs e)
        {
            
        }
        private async Task Kaydet(Articles model)
        {
            await manager.KaydetAsenkron(model);
        }
    }
}