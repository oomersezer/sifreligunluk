﻿using SifreliGunluk.Models;
using SifreliGunluk.Provider;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SifreliGunluk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class YeniKullaniciEkleSayfa : ContentPage
    {
        List<User> listKullanici = new List<User>();
        UserManager manager;
        public YeniKullaniciEkleSayfa()
        {
            manager = UserManager.DefaultManager;
            InitializeComponent();
            // Büyük harf ile başlaması için keyboard tipni değiştiriyorum
            entryAd.Keyboard = Keyboard.Chat;
            entrySoyad.Keyboard = Keyboard.Chat;
            entryKullaniciAdi.Keyboard = Keyboard.Chat;
            pickerCinsiyet.Items.Add("Erkek");
            pickerCinsiyet.Items.Add("Kadın");
            pickerCinsiyet.Items.Add("Belirtmek İstemiyorum");
            pickerCinsiyet.Title = "Cinsiyetinizi Seçin";
            entryMail.Keyboard = Keyboard.Email;
            KullanicilariGetir();

        }

        private void buttonKaydet_Clicked(object sender, EventArgs e)
        {

            foreach (User item in listKullanici)
            {
                if (item.Username==entryKullaniciAdi.Text)
                {
                    DisplayAlert("HATA", "Girmiş olduğunuz kullanıcı adı zaten kullanılıyor. Şütfen başka birşeyler deneyin.", "OK");
                    return;
                }
            }
            if (entrySifre.Text==entrySifreOnay.Text)
            {
                User yeniKullanici = new User
                {
                    Name = entryAd.Text,
                    Surname = entrySoyad.Text,
                    Username = entryKullaniciAdi.Text,
                    Age = pickerDogumTarihi.Date.ToString(),
                    Gender = pickerCinsiyet.SelectedItem.ToString(),
                    Mail = entryMail.Text,
                    Password = entrySifre.Text
                };
                Kaydet(yeniKullanici);
                Navigation.PopModalAsync();
            }
            else
            {
                DisplayAlert("Hata", "Girmiş olduğunuz şifreler uyuşmuyor", "OK");

            }
           
        }
        private async Task Kaydet(User model)
        {
            await manager.KaydetAsenkron(model);

        }

       
        private async Task KullanicilariGetir()
        {
            listKullanici = await manager.ListeOlarakGetir();
        }
    }
}