﻿using SifreliGunluk.Models;
using SifreliGunluk.Provider;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SifreliGunluk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AnaSayfaMaster : ContentPage
    {
        public static string username = GirisSayfa.userName;
        List<User> userList = new List<User>();
        UserManager manager;
        public AnaSayfaMaster()
        {
            manager = UserManager.DefaultManager;
            Listele();
            
            InitializeComponent();

        }
        private async Task Listele()
        {
            userList = await manager.ListeOlarakGetir();
            foreach (User item in userList)
            {
                if (item.Username == username)
                {
                    labelBaslik.Text = "Hoş Geldiniz " + item.Username + " !";
                    labelAd.Text = item.Name;
                    labelSoyad.Text = item.Surname;
                    labelYas.Text = item.Age;
                    labelCinsiyet.Text = item.Gender;
                    labelMail.Text = item.Mail;
                }
            }
        }
        
    }
}