﻿using SifreliGunluk.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SifreliGunluk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AnaSayfaDetail : ContentPage
    {
        ArticleManager manager;
        public AnaSayfaDetail()
        {
            manager = ArticleManager.DefaultManager;
            InitializeComponent();
            Listele();
        }
        private async Task Listele()
        {
            listYazilar.BindingContext = await manager.GetirAsenkron();
        }

        private void buttonYeniYazi_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new YeniYaziEkleSayfa());

        }
    }
}