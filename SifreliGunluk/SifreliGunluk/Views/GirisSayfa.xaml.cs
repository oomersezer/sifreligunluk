﻿using SifreliGunluk.Models;
using SifreliGunluk.Provider;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SifreliGunluk.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GirisSayfa : ContentPage
    {
        public static string userName;
        UserManager manager;
        List<User> listUser = new List<User>();
        public GirisSayfa()
        {
            manager = UserManager.DefaultManager;
            InitializeComponent();
            KullanicilariListele();
        }

        private void buttonGirisYap_Clicked(object sender, EventArgs e)
        {
           
            foreach (User item in listUser)
            {
                if (item.Username==entryKullaniciAdi.Text && item.Password==entrySifre.Text)
                {
                    userName = item.Username;

                    Navigation.PushModalAsync(new AnaSayfa());

                }
            }
        }
        private async Task KullanicilariListele()
        {
            listUser = await manager.ListeOlarakGetir();
        }
       
        private void buttonYeniKullanici_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new YeniKullaniciEkleSayfa());

        }
    }
}