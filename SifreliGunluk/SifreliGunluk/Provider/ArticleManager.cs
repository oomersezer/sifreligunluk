﻿using Microsoft.WindowsAzure.MobileServices;
using SifreliGunluk.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SifreliGunluk.Provider
{
    public partial class ArticleManager
    {
        static ArticleManager defaultInstance = new ArticleManager();
        MobileServiceClient client;

        IMobileServiceTable<Articles> articleTable;

        const string offlineDbPath = @"localstore.db";

        private ArticleManager()
        {
            this.client = new MobileServiceClient(Constants.ApplicationURL);

#if  OFFLINE_SYNC_ENABLED
             var store = new MobileServiceSQLiteStore(offlineDbPath);
            store.DefineTable<UsersModel>();

            //Initializes the SyncContext using the default IMobileServiceSyncHandler.
            this.client.SyncContext.InitializeAsync(store);

            this.todoTable = client.GetSyncTable<UsersModel>();
#else
            this.articleTable = client.GetTable<Articles>();
#endif
        }

        public static ArticleManager DefaultManager
        {
            get
            {
                return defaultInstance;
            }
            private set
            {
                defaultInstance = value;
            }
        }
        public MobileServiceClient CurrentClient
        {
            get { return client; }
        }

        public bool IsOfflineEnabled
        {
            get { return articleTable is Microsoft.WindowsAzure.MobileServices.Sync.IMobileServiceSyncTable<ArticleManager>; }
        }

        // Getirme işlemlerini gerçekleştiriyoruz
        public async Task<ObservableCollection<Articles>> GetirAsenkron(bool syncItem = false)
        {
            try
            {
#if OFFLINE_SYNC_ENABLED
                if (syncItems)
                {
                    await this.SyncAsync();
                }
#endif
                IEnumerable<Articles> items = await articleTable.ToEnumerableAsync();
                return new ObservableCollection<Articles>(items);


            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }

        //Kullanıcıları Liste şeklinde getireceğiz

        public async Task<List<Articles>> ListeOlarakGetir()
        {
            List<Articles> yaziListesi = await articleTable.ToListAsync();
            return yaziListesi;
        }
        // Güncellem eişlemlerini yapıyoruz
        public async Task Guncelle(Articles item)
        {
            await articleTable.UpdateAsync(item);

        }

        //Silme işlemlerini gerçekletiriyoruz
        public async Task Sil(Articles model)
        {
            await articleTable.DeleteAsync(model);

        }

        //Asenkron olarak veri tabanına kaydetme işlemlerini gerçekletriyoruz
        public async Task KaydetAsenkron(Articles item)
        {
            if (item.Id == null)
            {
                await articleTable.InsertAsync(item);
            }
            else
            {
                await articleTable.UpdateAsync(item);
            }
        }

        // Kullanici adindan bulma işlemlerini yapıyoruz
        public async Task<ObservableCollection<Articles>> KullaniciBul(string username)
        {
            IEnumerable<Articles> items = await articleTable.Where(x => x.Username == username).ToEnumerableAsync();
            return new ObservableCollection<Articles>(items);

        }

        // Eğer offline isek ne yapacağımızı bildiriyoruz
#if OFFLINE_SYNC_ENABLED
        public async Task SyncAsync()
        {
            ReadOnlyCollection<MobileServiceTableOperationError> syncErrors = null;

            try
            {
                await this.client.SyncContext.PushAsync();

                await this.todoTable.PullAsync(
                    //The first parameter is a query name that is used internally by the client SDK to implement incremental sync.
                    //Use a different query name for each unique query in your program
                    "allTodoItems",
                    this.todoTable.CreateQuery());
            }
            catch (MobileServicePushFailedException exc)
            {
                if (exc.PushResult != null)
                {
                    syncErrors = exc.PushResult.Errors;
                }
            }

            // Simple error/conflict handling. A real application would handle the various errors like network conditions,
            // server conflicts and others via the IMobileServiceSyncHandler.
            if (syncErrors != null)
            {
                foreach (var error in syncErrors)
                {
                    if (error.OperationKind == MobileServiceTableOperationKind.Update && error.Result != null)
                    {
                        //Update failed, reverting to server's copy.
                        await error.CancelAndUpdateItemAsync(error.Result);
                    }
                    else
                    {
                        // Discard local change.
                        await error.CancelAndDiscardItemAsync();
                    }

                    Debug.WriteLine(@"Error executing sync operation. Item: {0} ({1}). Operation discarded.", error.TableName, error.Item["id"]);
                }
            }
        }
#endif
    }
}
