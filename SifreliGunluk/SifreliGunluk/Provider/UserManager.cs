﻿using Microsoft.WindowsAzure.MobileServices;
using SifreliGunluk.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#if OFFLINE_SYNC_ENABLED
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using Microsoft.WindowsAzure.MobileServices.Sync;
#endif
namespace SifreliGunluk.Provider
{
    public partial class UserManager
    {
        static UserManager defaultInstance = new UserManager();
        MobileServiceClient client;

        IMobileServiceTable<User> userTable;

        const string offlineDbPath = @"localstore.db";

        private UserManager()
        {
            this.client = new MobileServiceClient(Constants.ApplicationURL);

#if  OFFLINE_SYNC_ENABLED
             var store = new MobileServiceSQLiteStore(offlineDbPath);
            store.DefineTable<UsersModel>();

            //Initializes the SyncContext using the default IMobileServiceSyncHandler.
            this.client.SyncContext.InitializeAsync(store);

            this.todoTable = client.GetSyncTable<UsersModel>();
#else
            this.userTable = client.GetTable<User>();
#endif
        }

        public static UserManager DefaultManager
        {
            get
            {
                return defaultInstance;
            }
            private set
            {
                defaultInstance = value;
            }
        }
        public MobileServiceClient CurrentClient
        {
            get { return client; }
        }

        public bool IsOfflineEnabled
        {
            get { return userTable is Microsoft.WindowsAzure.MobileServices.Sync.IMobileServiceSyncTable<User>; }
        }

        // Getirme işlemlerini gerçekleştiriyoruz
        public async Task<ObservableCollection<User>> GetirAsenkron(bool syncItem = false)
        {
            try
            {
#if OFFLINE_SYNC_ENABLED
                if (syncItems)
                {
                    await this.SyncAsync();
                }
#endif
                IEnumerable<User> items = await userTable.Where(x => !x.Done).ToEnumerableAsync();
                return new ObservableCollection<User>(items);


            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }

        //Kullanıcıları Liste şeklinde getireceğiz

        public async Task<List<User>> ListeOlarakGetir()
        {
            List<User> kullaniciListesi = await userTable.ToListAsync();
            return kullaniciListesi;
        }
        // Güncellem eişlemlerini yapıyoruz
        public async Task Guncelle(User item)
        {
            await userTable.UpdateAsync(item);

        }

        //Silme işlemlerini gerçekletiriyoruz
        public async Task Sil(User model)
        {
            await userTable.DeleteAsync(model);

        }

        //Asenkron olarak veri tabanına kaydetme işlemlerini gerçekletriyoruz
        public async Task KaydetAsenkron(User item)
        {
            if (item.Id == null)
            {
                await userTable.InsertAsync(item);
            }
            else
            {
                await userTable.UpdateAsync(item);
            }
        }

        // Kullanici adindan bulma işlemlerini yapıyoruz
        public async Task<ObservableCollection<User>> KullaniciBul(string username)
        {
            IEnumerable<User> items = await userTable.Where(x => x.Username == username).ToEnumerableAsync();
            return new ObservableCollection<User>(items);

        }

        // Eğer offline isek ne yapacağımızı bildiriyoruz
#if OFFLINE_SYNC_ENABLED
        public async Task SyncAsync()
        {
            ReadOnlyCollection<MobileServiceTableOperationError> syncErrors = null;

            try
            {
                await this.client.SyncContext.PushAsync();

                await this.todoTable.PullAsync(
                    //The first parameter is a query name that is used internally by the client SDK to implement incremental sync.
                    //Use a different query name for each unique query in your program
                    "allTodoItems",
                    this.todoTable.CreateQuery());
            }
            catch (MobileServicePushFailedException exc)
            {
                if (exc.PushResult != null)
                {
                    syncErrors = exc.PushResult.Errors;
                }
            }

            // Simple error/conflict handling. A real application would handle the various errors like network conditions,
            // server conflicts and others via the IMobileServiceSyncHandler.
            if (syncErrors != null)
            {
                foreach (var error in syncErrors)
                {
                    if (error.OperationKind == MobileServiceTableOperationKind.Update && error.Result != null)
                    {
                        //Update failed, reverting to server's copy.
                        await error.CancelAndUpdateItemAsync(error.Result);
                    }
                    else
                    {
                        // Discard local change.
                        await error.CancelAndDiscardItemAsync();
                    }

                    Debug.WriteLine(@"Error executing sync operation. Item: {0} ({1}). Operation discarded.", error.TableName, error.Item["id"]);
                }
            }
        }
#endif
    }
}
